﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kursovaya_v1
{
    class Visualization
    {
        Books books;
        protected Books Books { get => books; set => books = value; }
        protected void Sort(List<Tuple<string, int>> values, int typeOfSorting)
        {
            //сортировка в алфавитном порядке
            if (typeOfSorting == 0) values.Sort((x, y) => x.Item1.CompareTo(y.Item1));
            else values.Sort((x, y) => y.Item2.CompareTo(x.Item2));

        }
        /// <summary>
        /// создать распределение
        /// </summary>
        protected List<Tuple<string, int>> Source(string request, int typeOfSorting)
        {
            var values = new List<Tuple<string, int>>();
            if (Books != null)
                foreach (Book book in Books)
                {
                    string tagvalue = book.GetChangedValue(request);
                    tagvalue = (request == "address" || request == "location") && tagvalue != null ? book.GetChangedValue(request).Split(',')[0] : book.GetChangedValue(request);
                    //если получили значение по тэгу (тэг есть)
                    if (tagvalue != null)
                    {
                        var index = values.FindIndex(x => x.Item1 == tagvalue);
                        if (index == -1)
                            values.Add(new Tuple<string, int>(tagvalue, 1));
                        else values[index] = new Tuple<string, int>(values[index].Item1, values[index].Item2 + 1);
                    }
                }
            Sort(values, typeOfSorting);
            return values;
        }
    }
}
