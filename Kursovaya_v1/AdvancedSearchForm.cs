﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursovaya_v1
{
    public partial class AdvancedSearchForm : Form
    {
        List<string> oldSearch = null;
        List<OneTag> tagsArray = new List<OneTag>();
        bool closeForm = true;
        public AdvancedSearchForm(HashSet<string> allTags, List<string> oldSearch)
        {
            InitializeComponent();

            this.oldSearch = oldSearch;
            tagsArray.Add(new OneTag("any field"));
            var temp = new List<OneTag>();
            foreach (var tag in allTags)
            {
                temp.Add(new OneTag(tag));
            }
            temp.Sort();
            tagsArray.AddRange(temp);

            btOK.DialogResult = DialogResult.OK;
            InitializeDGV();
        }
        private void InitializeDGV()
        {

            dgvSearch.Columns.Add("col1", "");
            dgvSearch.Columns["col1"].ReadOnly = true;

            var col2 = new DataGridViewComboBoxColumn();
            col2.DataSource = tagsArray;
            col2.DisplayMember = col2.ValueMember = "DisplayMem";
            dgvSearch.Columns.Add(col2);

            var col3 = new DataGridViewComboBoxColumn();
            col3.Items.AddRange(new string[] { "matches all", "matches any", "matches none" });
            dgvSearch.Columns.Add(col3);

            dgvSearch.Columns.Add("col4", "");    //of the following words or phrase:
            dgvSearch.Columns["col4"].ReadOnly = true;

            dgvSearch.Columns.Add("col5", "");    //вводимая фраза

            dgvSearch.Columns.Add(new DataGridViewComboBoxColumn
            {
                Items = { "Intersect", "Union", "Minus" }
            });

            dgvSearch.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvSearch.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;


            if (oldSearch != null && oldSearch.Count != 0)
                for (int i = 0; i < oldSearch.Count / 4; i++)
                {
                    dgvSearch.Rows.Add();
                    dgvSearch[1, i].Value = oldSearch[i * 4 + 0];
                    dgvSearch[2, i].Value = oldSearch[i * 4 + 1];
                    dgvSearch[4, i].Value = oldSearch[i * 4 + 2];
                    dgvSearch[5, i].Value = oldSearch[i * 4 + 3];
                }
        }

        private void DgvSearch_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dgvSearch[0, e.RowIndex].Value = "where";
            if (dgvSearch.Rows.Count > 1)
            {
                dgvSearch[3, e.RowIndex].Value = "of the following words or phrase:";
                if (oldSearch == null || oldSearch.Count == 0 || dgvSearch.Rows.Count >= oldSearch.Count / 4)
                {
                    dgvSearch[1, e.RowIndex].Value = "any field";
                    dgvSearch[2, e.RowIndex].Value = "matches all";
                    dgvSearch[5, e.RowIndex].Value = "Intersect";
                }
            }
        }

        //при создании таблицы событие DgvSearch_RowsAdded вызывается при добавлении первой строки
        private void DgvSearch_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            switch (e.Column.Index)
            {
                case 2: dgvSearch[1, dgvSearch.Rows.Count - 1].Value = "any field"; break;
                case 3: dgvSearch[2, dgvSearch.Rows.Count - 1].Value = "matches all"; break;
                case 4: dgvSearch[3, dgvSearch.Rows.Count - 1].Value = "of the following words or phrase:"; break;
                case 5: dgvSearch[5, dgvSearch.Rows.Count - 1].Value = "Intersect"; break;
            }
            e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        public List<string> GetResult()
        {
            var res = new List<string>();
            int rowsCount = dgvSearch.Rows.Count;
            for (int i = 0; i < rowsCount - 1; i++)
            {
                res.Add((string)dgvSearch[1, i].Value);
                res.Add((string)dgvSearch[2, i].Value);
                res.Add((string)dgvSearch[4, i].Value);
                res.Add((string)dgvSearch[5, i].Value);
            }
            return res;
        }

        private void BtOK_Click(object sender, EventArgs e)
        {
            closeForm = true;
            bool isFill = true;
            if (dgvSearch.Rows.Count == 1) isFill = false;
            for (int i = 0; i < dgvSearch.Rows.Count - 1 && isFill; i++)
            {
                if (String.IsNullOrWhiteSpace((string)dgvSearch[4, i].Value))
                {
                    isFill = false;
                }
            }
            if (!isFill)
            {
                MessageBox.Show("Заполните все поля", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                closeForm = false;
            }
        }

        private void AdvancedSearch_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!closeForm) e.Cancel = true;
            closeForm = true;
        }
    }
}
