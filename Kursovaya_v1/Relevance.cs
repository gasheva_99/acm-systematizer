﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursovaya_v1
{
    /// <summary>
    /// Происходит следующая проверка
    /// 1) кол-во страниц >=3 или тэга страниц может вообще не быть (?)
    /// 2) id
    /// </summary>
    static class Relevance
    {
        public static bool Relevant(Book book)
        {
            //проверка 1 
            if (book.GetChangedValue("numpages") == null || Int32.Parse(book.GetChangedValue("numpages")) < 3) return false;
            //проверка 2


            return true;
        }
    }
}
