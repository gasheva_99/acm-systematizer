﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Kursovaya_v1
{
    class Table: Visualization
    {
        System.Windows.Forms.DataGridView dgvVisualization;
        public Table(Books books, DataGridView dgvVisualization)
        {
            this.Books = books;
            this.dgvVisualization = dgvVisualization;
            Initialize();
        }
        private void Initialize()
        {
            dgvVisualization.ColumnCount = 2;
            dgvVisualization.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvVisualization.Columns[1].HeaderText = "Кол-во";
        }
        
        /// <summary>
        /// изменить заполнение таблицы
        /// </summary>
        public void ChangeTable(string request, string columnName, int sortType)
        {
            dgvVisualization.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgvVisualization.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
            dgvVisualization.Rows.Clear();
            dgvVisualization.Columns[0].HeaderText = columnName; 
            var source = Source(request, sortType);
            foreach(var book in source)
            {
                dgvVisualization.Rows.Add(book.Item1, book.Item2);
            }
        }
        

    }
}
