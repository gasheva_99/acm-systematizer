﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Kursovaya_v1
{
    public partial class MainForm : Form
    {
        Books allBooks = new Books();
        Books curBooks;
        List<string> userSearch = null;
        Dictionary<String, String> abbr = new Dictionary<String, String>();
        HashSet<string> allTags = new HashSet<string>();    //все тэги (для поиска)
        Form waitingForm;
        ProgressBar pBar;
        //BackgroundWorker backgroundWorker = new BackgroundWorker();

        private void EnableElements()
        {
            if (allBooks == null || allBooks.size() == 0)
            {
                расширенныйПоискToolStripMenuItem.Enabled = false;
                анализToolStripMenuItem.Enabled = false;
                сохранитьToolStripMenuItem.Enabled = false;
                выводToolStripMenuItem.Enabled = false;
                btSearch.Enabled = false;
                btDrop.Enabled = false;
                tbSearch.Enabled = false;
            }
            if (allBooks != null && allBooks.size() > 0)
            {
                расширенныйПоискToolStripMenuItem.Enabled = true;
                анализToolStripMenuItem.Enabled = true;
                сохранитьToolStripMenuItem.Enabled = true;
                выводToolStripMenuItem.Enabled = true;
                btSearch.Enabled = true;
                btDrop.Enabled = true;
                tbSearch.Enabled = true;
            }
        }
        public MainForm()
        {
            //String.Compare(val1, val2, true);
            //(in)proceeedings - труды конференции
            //article - журнал
            curBooks = allBooks;
            InitializeComponent();
            InitializeDGV();
            int yearNow = DateTime.Now.Year;
            for (int i = yearNow - 6; i <= yearNow; i++)
            {
                maxYear.Items.Add(i);
                minYear.Items.Add(i);
            }
            maxYear.SelectedIndex = maxYear.Items.Count - 1;
            maxYear.Tag = maxYear.Items.Count - 1;  //индекс последнего выбранного значение
            minYear.SelectedIndex = 0;
            minYear.Tag = 0;
            lblResultCount.Text = "0";
            EnableElements();

            createAbbrList();
        }

        private void InitializeDGV()
        {
            dgvBooks.Columns.Add("title", "Заглавие");
            dgvBooks.Columns["title"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private void MsLoad_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "Bibtex file (*.bib)|*.bib";
            dialog.InitialDirectory = @"C:\Users\Таня\Documents\Лекции\Курсовая\acm — копия.bib"; //@"C:\";
            dialog.Title = "Please select a bibtex file to load";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    allBooks = new Books();
                    BibTexFile.ReadFromBibtex(dialog.FileName, allTags, allBooks);
                    if (allBooks == null || allBooks.size() == 0)
                    {
                        MessageBox.Show("Данные не загружены");
                    }
                    FillDGV(allBooks);
                    curBooks = allBooks;
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Данные не загружены\n" + exc.Message);
                    return;
                }
            }
            if (allBooks == null || allBooks.size() == 0) MessageBox.Show("Данные не загружены");
            FillDGV(allBooks);
            curBooks = allBooks;
            EnableElements();



            //WordFile.WriteInFile(@"C:\Users\Таня\source\repos\Kursovaya_v1\1.docx", curBooks);
        }
        private void FillDGV(Books books)
        {
            Console.WriteLine("Filling dgv...");
            dgvBooks.Rows.Clear();
            lblResultCount.Text = "0";
            int i = 0;

            if (books != null && books.size() > 0)
            {
                foreach (Book book in books)
                {
                    int bookYear;
                    Int32.TryParse(book.GetOriginalValue("year"), out bookYear);
                    //если книга подходит по временным рамкам
                    if (bookYear >= (int)minYear.SelectedItem && bookYear <= (int)maxYear.SelectedItem)
                    {
                        dgvBooks.Rows.Add(book.GetOriginalValue("title"));
                        dgvBooks[0, i].Tag = book.ID;
                        i++;
                    }
                }
                if (dgvBooks.Rows.Count == 0)
                {
                    FilltbBook(null);
                    return;
                }
                FilltbBook(books[0]);
                lblResultCount.Text = dgvBooks.Rows.Count.ToString();
            }
            else
            {
                lblResultCount.Text = "0";
                FilltbBook(null);
            }
        }
        private void СохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (curBooks == null || curBooks.size() == 0)
            {
                MessageBox.Show("Нет данных для сохранения", "Сохранение", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string path = "";
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = "Bibtex Format (*.bib)|*.bib";
                if (sfd.ShowDialog() == DialogResult.OK)
                    path = sfd.FileName;
            }
            BibTexFile.WriteInBibtex(path, curBooks);


        }

        private void DgvBooks_SelectionChanged(object sender, EventArgs e)
        {
            int rowIndex = dgvBooks.CurrentRow.Index;

            Console.WriteLine("Finding book...");
            if (dgvBooks.SelectedRows.Count > 0)
            {
                if (dgvBooks[0, rowIndex].Tag != null) FilltbBook(curBooks.GetBookById((string)dgvBooks[0, rowIndex].Tag));
            }

        }
        private void FilltbBook(Book book)
        {
            //tbBook.ut
            if (book == null)
            {
                tbBook.Clear();
                return;
            }
            string text = "";
            foreach (var tag in book.TagValue)
                text += tag.Key + " = " + tag.Value.Item1 + '\n';
            tbBook.Text = text;
        }

        SearchByWord<Tuple<string, string>> searchByWord = new SearchByWord<Tuple<string, string>>();
        int sum = 0;
        private void BtSearch_Click(object sender, EventArgs e)
        {
            searchByWord.setParameters(new Tuple<string, string>(tbSearch.Text, "any field"));
            curBooks = searchByWord.Search(allBooks);
            tbBook.Clear();
            FillDGV(curBooks);
            userSearch = null;


        }

        private void BtDrop_Click(object sender, EventArgs e)
        {
            curBooks = allBooks;
            tbSearch.Clear();
            FillDGV(allBooks);
            userSearch = null;
        }

        private Books SearchByYear(Books curBooks)
        {
            var books = new Books();
            foreach (Book book in curBooks)
            {
                int bookYear;
                Int32.TryParse(book.GetOriginalValue("year"), out bookYear);
                //если книга подходит по временным рамкам
                if (bookYear >= (int)minYear.SelectedItem && bookYear <= (int)maxYear.SelectedItem)
                {
                    books.AddBook(book);
                }
            }
            return books;
        }
        private void ДиаграммаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new DiagramForm(SearchByYear(curBooks));
            form.Owner = this;
            form.ShowDialog();
        }

        AdvancedSearch<List<string>> advancedSearch = new AdvancedSearch<List<string>>();
        private void РасширенныйПоискToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new AdvancedSearchForm(allTags, userSearch);
            form.Owner = this;
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {

                userSearch = form.GetResult();
                advancedSearch.setParameters(userSearch);
                curBooks = advancedSearch.Search(allBooks);
                FillDGV(curBooks);
            }
        }

        private void Years_SelectedValueChanged(object sender, EventArgs e)
        {
            var curCB = sender as ComboBox;
            //если нижняя граница больше верхней
            if (maxYear.SelectedIndex < minYear.SelectedIndex)
            {
                //возвращаем старые значения
                maxYear.SelectedIndex = (int)maxYear.Tag;
                minYear.SelectedIndex = (int)minYear.Tag;
            }
            else
            {
                maxYear.Tag = maxYear.SelectedIndex;
                minYear.Tag = minYear.SelectedIndex;
            }
            FillDGV(curBooks);
            //SearchByYears();
        }

        private void TbSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btSearch.PerformClick();
            }
        }

        private void ВыходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private bool useAbbr = true;
        private void ВыводToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var bw = new BackgroundWorker();
            if (curBooks == null || curBooks.size() == 0)
            {
                MessageBox.Show("Нет данных для вывода", "Вывод", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            var dialog = MessageBox.Show("Вывести по стандарту IEEE? Иначе вывод будет осуществлен по ГОСТу", "Вывод по стандарту", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            if (dialog == DialogResult.Cancel) return;

            curBooks = SearchByYear(curBooks);
            useAbbr = true;
            if (dialog == DialogResult.Yes)
            {
                var abbrForm = MessageBox.Show("Использовать сокращения?", "Сокращения", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                if (abbrForm == DialogResult.No) useAbbr = false;
                bw.DoWork += bw_DoWorkIEEE;
            }
            if (dialog == DialogResult.No) bw.DoWork += bw_DoWorkGost;
            InitializationWaitingForm();
            waitingForm.Show();

            bw.RunWorkerCompleted += bw_RunWorkerCompleted;
            bw.RunWorkerAsync();
        }
        private void bw_DoWorkIEEE(object sender, DoWorkEventArgs e)
        {
            WordFile.IEEE("", curBooks, abbr, useAbbr);
        }
        private void bw_DoWorkGost(object sender, DoWorkEventArgs e)
        {
            WordFile.Gost("", curBooks);
        }


        private void createAbbrList()
        {
            try
            {
                using (StreamReader sr = new StreamReader("abbr.txt"))
                {
                    while (!sr.EndOfStream)
                    {
                        string curLine = sr.ReadLine();
                        var abbrStr = curLine.Split(' ');
                        abbr.Add(abbrStr[0].Trim(), abbrStr[1].Trim());

                    }
                }
            }

            catch (Exception e)
            {
                abbr = null;
                Console.WriteLine("Проблема с файлом аббревиатур");
                Console.WriteLine(e.Message);
                return;
            }
        }
        private void OnLostFocus(object sender, EventArgs e)
        {
            base.OnLostFocus(e);
            waitingForm.Focus();
        }
        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pBar.MarqueeAnimationSpeed = 0;
            pBar.Style = ProgressBarStyle.Blocks;
            pBar.Value = pBar.Minimum;
            waitingForm.Close();
            waitingForm.Dispose();

        }
        private void InitializationWaitingForm()
        {
            waitingForm = new Form();
            waitingForm.Owner = this;
            waitingForm.FormBorderStyle = FormBorderStyle.Sizable;
            waitingForm.ControlBox = false;
            waitingForm.StartPosition = FormStartPosition.CenterScreen;

            waitingForm.LostFocus += OnLostFocus;
            waitingForm.TopMost = true;

            waitingForm.Width = 200;
            waitingForm.Height = 100;

            Label label = new Label();
            label.Text = "Ожидание завершения операции";
            label.Dock = DockStyle.Top;
            waitingForm.Controls.Add(label);



            pBar = new ProgressBar();
            pBar.Dock = DockStyle.Bottom;
            pBar.Style = ProgressBarStyle.Marquee;
            pBar.MarqueeAnimationSpeed = 50;
            waitingForm.Controls.Add(pBar);
            waitingForm.Visible = false;
        }
    }


}
