﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kursovaya_v1
{
    interface ISearch<T>
    {
        void setParameters(T parameter);
        Books Search(Books b);
    }
}
