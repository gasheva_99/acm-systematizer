﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Excel = Microsoft.Office.Interop.Excel;
using Word = Microsoft.Office.Interop.Word;

namespace Kursovaya_v1
{
    public partial class DiagramForm : Form
    {
        Books books;
        int sortType = 0;
        enum TypeOfVisualization { diagramm, table };
        TypeOfVisualization selectedType = TypeOfVisualization.diagramm;
        Diagram diagram;
        Table table;
        Form waitingForm;
        ProgressBar pBar;
        bool withChart = false;

        public DiagramForm(Books books)
        {
            InitializeComponent();
            this.books = books;
            diagram = new Diagram(chart1, books);
            table = new Table(books, dgvVisualization);
            Initialization();
        }
        private void Initialization()
        {
            cbType.Items.AddRange(new String[] { "Круговая", "Столбчатая", "Линия" });
            cbParameter.Items.AddRange(new String[] { "Адрес", "Год", "Издательство", "Место проведения (для конференций)", "Тип публикации" });
            cbType.SelectedIndex = 0;
            cbParameter.SelectedIndex = 0;
            cbSort.Items.AddRange(new String[] { "По алфавиту", "По кол-ву" });
            cbSort.SelectedIndex = 0;
            cbVisualization.Items.AddRange(new String[] { "Диаграмма", "Таблица" });
            cbVisualization.SelectedIndex = 0;
            btLabels.Text = "Скрыть названия";
            btCount.Text = "Показать кол-во";

        }
        private void InitializationWaitingForm()
        {
            waitingForm = new Form();
            waitingForm.Owner = this;
            waitingForm.FormBorderStyle = FormBorderStyle.Sizable;
            waitingForm.ControlBox = false;
            waitingForm.StartPosition = FormStartPosition.CenterScreen;

            waitingForm.LostFocus += OnLostFocus;
            waitingForm.TopMost = true;

            waitingForm.Width = 200;
            waitingForm.Height = 100;

            Label label = new Label();
            label.Text = "Ожидание завершения операции";
            label.Dock = DockStyle.Top;
            waitingForm.Controls.Add(label);



            pBar = new ProgressBar();
            pBar.Dock = DockStyle.Bottom;
            pBar.Style = ProgressBarStyle.Marquee;
            pBar.MarqueeAnimationSpeed = 50;
            waitingForm.Controls.Add(pBar);
            waitingForm.Visible = false;
        }

        private void OnLostFocus(object sender, EventArgs e)
        {
            base.OnLostFocus(e);
            waitingForm.Focus();
        }
        
        // получить тэг запроса
        private string RequestParam()
        {
            string request;
            switch (cbParameter.Text)
            {
                case "Адрес": request = "address"; break;
                case "Год": request = "year"; break;
                case "Издательство": request = "publisher"; break;
                case "Тип публикации": request = "publication type"; break;
                default: request = "location"; break;
            }
            return request;
        }

        private void CbParameter_SelectedIndexChanged(object sender, EventArgs e)
        {
            diagram.ZoomReset();
            if (selectedType == TypeOfVisualization.diagramm)
            {
                diagram.ChangeChart(RequestParam(), sortType);
            }
            else
            {
                table.ChangeTable(RequestParam(), (string)cbParameter.Text, sortType);
            }
        }

        private void CbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (selectedType == TypeOfVisualization.diagramm)
            {
                diagram.ZoomReset();
                diagram.ChangeType(cbType.Text);
                diagram.ChangeChart(RequestParam(), sortType);
                btLabels.Enabled = btCount.Enabled = true;
                if (diagram.ChartType == SeriesChartType.Pie) btLabels.Enabled = btCount.Enabled = false;
            }
        }

        private void BtLabelBottom_Click(object sender, EventArgs e)
        {
            bool enable = diagram.XLabelStyleEnabled;
            if (enable) btLabels.Text = "Показать названия";
            else btLabels.Text = "Скрыть названия";
            diagram.XLabelStyleEnabled = !enable;
        }

        private void BtCount_Click(object sender, EventArgs e)
        {
            bool enable = diagram.IsValueShownAsLabel;
            if (enable) btCount.Text = "Показать кол-во";
            else btCount.Text = "Скрыть кол-во";
            diagram.IsValueShownAsLabel = !enable;
        }

        private void CbSort_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbSort.SelectedIndex == 0) sortType = 0;
            else sortType = 1;
            if (selectedType == TypeOfVisualization.diagramm) diagram.ChangeChart(RequestParam(), sortType);
            else table.ChangeTable(RequestParam(), (string)cbParameter.Text, sortType);
        }

        private void CbVisualization_SelectedIndexChanged(object sender, EventArgs e)
        {
            // если выбрана таблица
            if (cbVisualization.SelectedIndex == 1)
            {
                selectedType = TypeOfVisualization.table;
                cbType.Visible = false;
                btLabels.Visible = false;
                btCount.Visible = false;
                //cbSort.Visible = false;
                toolStripSeparator3.Visible = false;
                toolStripSeparator1.Visible = false;
                chart1.Hide();
                dgvVisualization.Visible = true;
                table.ChangeTable(RequestParam(), (string)cbParameter.Text, sortType);
            }
            // если выбрана диаграмма
            else
            {
                selectedType = TypeOfVisualization.diagramm;
                cbType.Visible = true;
                btLabels.Visible = true;
                btCount.Visible = true;
                cbSort.Visible = true;
                toolStripSeparator1.Visible = true;
                toolStripSeparator1.Visible = true;

                dgvVisualization.Hide();
                chart1.Visible = true;
                diagram.ChangeChart(RequestParam(), sortType);
            }
        }
        // Экспорт в Word/Excel
        private void ToolStripButton1_Click(object sender, EventArgs e)
        {
            if (books.size() == 0)
            {
                MessageBox.Show("Нет данных для построения", "", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                return;
            }
            var bt = sender as ToolStripButton;
            var bw = new BackgroundWorker();
            if (bt.Name == "btExcel")
            {
                var dialogResult = MessageBox.Show("Построить дополнительно диаграмму?", "Экспорт", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                withChart = dialogResult == DialogResult.Yes;
                bw.DoWork += bw_DoWorkExcel;

            }
            else bw.DoWork += bw_DoWorkWord;

            InitializationWaitingForm();
            waitingForm.Show();

            table.ChangeTable(RequestParam(), (string)cbParameter.Text, sortType);

            
            bw.RunWorkerCompleted += bw_RunWorkerCompleted;
            bw.RunWorkerAsync();
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pBar.MarqueeAnimationSpeed = 0;
            pBar.Style = ProgressBarStyle.Blocks;
            pBar.Value = pBar.Minimum;
            waitingForm.Close();
            waitingForm.Dispose();

        }

        private void bw_DoWorkWord(object sender, DoWorkEventArgs e)
        {
            WordFile.Export_Data_To_Word(dgvVisualization, "newFile.doc");
        }
        private void bw_DoWorkExcel(object sender, DoWorkEventArgs e)
        {

            ExcelFile.ExportTableToExcel(dgvVisualization, withChart);
        }

        
    }
}
