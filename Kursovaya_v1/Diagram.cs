﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;

namespace Kursovaya_v1
{
    class Diagram: Visualization
    {
        Chart chart1;
        bool isValueShownAsLabel;
        bool xLabelStyleEnabled;
        
        public bool IsValueShownAsLabel { get => isValueShownAsLabel; set => isValueShownAsLabel = chart1.Series["Chart"].IsValueShownAsLabel = value; }
        public bool XLabelStyleEnabled { get => xLabelStyleEnabled; set => xLabelStyleEnabled = chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.Enabled = value; }
        public SeriesChartType ChartType { get => chart1.Series["Chart"].ChartType; }

        public Diagram(Chart chart, Books books)
        {
            this.chart1 = chart;
            this.Books = books;
            Initialize();
        }
        private void Initialize ()
        {
            // расположение подписей
            chart1.ChartAreas[0].AxisX.LabelStyle.Angle = -90;
            chart1.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.LightGray;
            chart1.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.LightGray;
            chart1.Series.Add("Chart");

            chart1.Series["Chart"].ToolTip = "#VALX; Кол-во = #VALY";
        }
        
        //сброс зума
        public void ZoomReset()
        {
            chart1.ChartAreas[0].AxisX.ScaleView.ZoomReset();
            chart1.ChartAreas[0].AxisY.ScaleView.ZoomReset();
        }
        /// <summary>
        /// изменить тип диаграммы
        /// </summary>
        public void ChangeType(string type)
        {
            chart1.Series.Clear();
            var chartType = SeriesChartType.Pie;
            switch (type)
            {
                case "Линия": chartType = SeriesChartType.Line; break;
                case "Круговая": chartType = SeriesChartType.Pie; break;
                case "Столбчатая": chartType = SeriesChartType.Column; break;
            }
            chart1.Series.Add(new Series("Chart")
            {
                ChartType = chartType
            });
        }
        public void ChangeChart(string request, int sortType)
        {
            var source = Source(request, sortType);

            chart1.ChartAreas["ChartArea1"].AxisX.Interval = 1;
            chart1.Series["Chart"]["PieLabelStyle"] = "Disabled";
            chart1.Series["Chart"].Points.DataBindXY(source.Select(x => x.Item1).ToList(), source.Select(x => x.Item2).ToList());

            //zoom
            chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
            chart1.ChartAreas[0].AxisX.ScrollBar.IsPositionedInside = true;
            chart1.ChartAreas[0].CursorY.IsUserEnabled = true;
            chart1.ChartAreas[0].CursorY.IsUserSelectionEnabled = true;
            chart1.ChartAreas[0].AxisY.ScrollBar.IsPositionedInside = true;

        }
        
    }
}
