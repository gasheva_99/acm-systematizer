﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Kursovaya_v1
{
    /// <summary>
    /// базовый объект - журнал, конференция, книга и тд
    /// </summary>
    public class Book
    {
        //ключ - тэг; значение - содержание тэга, преобразованное содержание для сравнения
        Dictionary<string, Tuple<string, string>> tagValue;
        string trash;
        string id;  //название + автор + кол-во страниц

        public Book()
        {
            tagValue = new Dictionary<string, Tuple<string, string>>();
        }
        public string ID
        {
            get => id;
            set => id = StringParser.Parser(value);
        }
        public string Trash
        {
            get => trash;
            set => trash = value;
        }
        public Dictionary<string, Tuple<string, string>> TagValue
        {
            get => tagValue;
        }
        /// <summary>
        /// Проверка находится ли данное слово в тэгах книги
        /// </summary>
        /// <param name="request"></param>
        /// <returns>true - если содержит; false - иначе</returns>
        public bool ContainsWord(string request, string reqtag)
        {
            var allValues = new List<string>();
            //поиск по всем полям
            if (reqtag == "any field")
            {
                allValues = tagValue.Values.Select(x => " " + StringParser.Parser(Regex.Replace(x.Item2.ToUpper(), "[^A-Z0-9]", " ")) + " ").ToList();
            }
            else
            {
                //поиск по конкретному полю
                if (!tagValue.ContainsKey(reqtag)) return false;
                allValues.Add(" " + StringParser.Parser(Regex.Replace(tagValue[reqtag].Item2.ToUpper(), "[^A-Z0-9]", " ")) + " ");
            }
            //поиск по фразе(слову). Фраза отделена справа и слева пробелами
            // получаем все значения тэгов, удалив все знаки препинания, кроме вайлдкардов
            request = StringParser.Parser(Regex.Replace(request, @"[^A-Za-z0-9\\'\+\?\*]", " "));

            request = @"\b" + request + @"\b|^" + request + @"\b|^" + request + @"$|\b" + request + @"$";
            Func<string, bool> search = x => Regex.IsMatch(x, request, RegexOptions.IgnoreCase);
            
            return allValues.Any(x => search(x));
        }
        /// <summary>
        /// Проверка есть ли полное совпадение по слову в тэгах книги
        /// </summary>
        /// <param name="request"></param>
        /// <returns>true - если содержит; false - иначе</returns>
        public bool ContainsCompleteWord(string request, string reqtag)
        {
            request = StringParser.Parser(request);
            request = @"^"+request+ @"$";
            Func<string, bool> search = x => Regex.IsMatch(x, request, RegexOptions.IgnoreCase);
            
            if (reqtag == "any field")
            {
                return tagValue.Values.Any(x => search(x.Item2));
            }
            else
            {
                if (!tagValue.ContainsKey(reqtag)) return false;                
                return search(tagValue[reqtag].Item2); 
            }
        }

        public void AddTag(string key, string val)
        {
            tagValue.Add(key, new Tuple<string, string>(val.Trim(), StringParser.Parser(val)));
        }

        public string GetOriginalValue(string key)
        {
            var tuple = new Tuple<string, string>("1", "2");
            if (tagValue.TryGetValue(key, out tuple)) return tuple.Item1;
            else return null;
        }
        public string GetChangedValue(string key)
        {
            Tuple<string, string> tuple = default;
            if (tagValue.TryGetValue(key, out tuple)) return tuple.Item2;
            return null;
        }
        
    }
}
