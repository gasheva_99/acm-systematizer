﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kursovaya_v1
{
    class AdvancedSearch<T>: ISearch<T>
    {
        List<string> request;
        /// <summary>
        /// нахождение списка книг по запросу
        /// </summary>
        /// <param name="request">заданное слово</param>
        /// <returns>Books или null</returns>
        public Books Search(Books b)
        {
            List<Book> books = b.getBooks;
            //если необходимого тэга нет, то книга не подходит
            if (request.Count < 4) return null;
            //тэг match запрос и\или
            string tag, link, match, word;
            List<Book> result = new List<Book>();
            tag = request[0];
            match = request[1];
            word = request[2];
            word = word.Replace("*", @"\w*");
            word = word.Replace("+", @"\w+");
            word = word.Replace("?", @"\w");
            switch (match)
            {
                case "matches all": result.AddRange(books.Where(x => x.ContainsCompleteWord(word, tag))); break; //полное совпадение
                case "matches any": result.AddRange(books.Where(x => x.ContainsWord(word, tag))); break;  //частичное совпадение
                                                                                                          //выбраем книги с тэгом. Вычитаем из них все, содержащие указанный тэг
                default:
                    if (tag != "any field")
                        result.AddRange(books.Where(x => x.GetChangedValue(tag) != null).Except(books.Where(x => x.ContainsWord(word, tag))));
                    else result.AddRange(books.Except(books.Where(x => x.ContainsWord(word, tag))));
                    break;//"matches none" 
            }
            //остальная часть результата запроса зависит от связки и\или
            for (int i = 3; i < request.Count - 1; i += 4)
            {
                link = request[i];
                tag = request[i + 1];
                match = request[i + 2];
                word = request[i + 3];
                word = word.Replace("*", @"\w*");
                word = word.Replace("+", @"\w+");
                word = word.Replace("?", @"\w");

                //или -> ищем во всех книгах 
                if (link == "Union")
                {
                    switch (match)
                    {
                        case "matches all": result.AddRange(books.Where(x => x.ContainsCompleteWord(word, tag))); break; //полное совпадение
                        case "matches any": result.AddRange(books.Where(x => x.ContainsWord(word, tag))); break;  //частичное совпадение
                                                                                                                  //выбраем книги с тэгом. Вычитаем из них все, содержащие указанный тэг
                        default:
                            if (tag != "any field")
                                result.AddRange(books.Where(x => x.GetChangedValue(tag) != null).Except(books.Where(x => x.ContainsWord(word, tag)))); //"matches none" отcутствует
                            else result.AddRange(books.Where(x => !x.ContainsWord(word, tag)));
                            break;
                    }
                }
                // и -> ищем в результате
                else
                if (link == "Intersect")
                {
                    //удаляем из результата книги, в которых нет указанного тэга
                    if (tag != "any field") result = result.Except(result.Where(x => x.GetChangedValue(tag) == null)).ToList();
                    switch (match)
                    {
                        //Вычитаем из результата все неподходящие книги
                        case "matches all": result = result.Except(result.Where(x => !x.ContainsCompleteWord(word, tag))).ToList(); break; //нет полного совпадения
                        case "matches any": result = result.Except(result.Where(x => !x.ContainsWord(word, tag))).ToList(); break;  //нет частичного совпадения
                        default:
                            result = result.Except(result.Where(x => x.ContainsWord(word, tag))).ToList(); break;//"matches none" отcутствует
                    }

                }
                else
                {
                    //удаляем из результата книги, в которых нет указанного тэга
                    if (tag != "any field") result = result.Except(result.Where(x => x.GetChangedValue(tag) == null)).ToList();
                    switch (match)
                    {
                        //Вычитаем из результата все неподходящие книги
                        case "matches all": result = result.Except(result.Where(x => x.ContainsCompleteWord(word, tag))).ToList(); break; //нет полного совпадения
                        case "matches any": result = result.Except(result.Where(x => x.ContainsWord(word, tag))).ToList(); break;  //нет частичного совпадения
                                                                                                                                   //Вычитаем из результата все книги, содержащие указанную фразу
                        default: result = result.Except(result.Where(x => !x.ContainsWord(word, tag))).ToList(); break;//"matches none" отcутствует
                    }

                }
            }
            //чистка результата от дубликатов
            var ids = result.Select(x => x.ID).ToList();
            foreach (var id in ids)
            {
                var bookdWithId = result.Where(x => x.ID == id).ToList();
                if (bookdWithId.Count() > 1) result.Remove(bookdWithId[0]);
            }
            if (result.Count() == 0) return null;
            else return new Books(result);
        }

        public void setParameters(T parameter)
        {
            request = parameter as List<String>;
        }
    }
}
