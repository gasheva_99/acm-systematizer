﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kursovaya_v1
{
    class OneTag: IComparable
    {
        public OneTag(string tag)
        {
            DisplayMem = ValueMem = tag;
        }
        public string DisplayMem { get; set; }
        public string ValueMem { get; set; }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;
            OneTag otherTag = obj as OneTag;
            if (otherTag != null)
                return this.DisplayMem.CompareTo(otherTag.DisplayMem);
            else throw new ArgumentException("Object is not a Tag");
        }
    }
}
