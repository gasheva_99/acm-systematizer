﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Kursovaya_v1
{
    //преобразование строк
    static class StringParser
    {
        //возвращает строку с удаленным двойными пробелами (табами) и 
        //удаленными пробельными символами в конце и начале строки
        //табы заменяются на пробелы
        
        public static string Parser(string str)
        {
            if (str==null) return null;
            str = Regex.Replace(str, "\\s+", " ");
            str = Regex.Replace(str, "\\t+", " ");
            str = str.Trim();
            return str;
        }
    }
}
