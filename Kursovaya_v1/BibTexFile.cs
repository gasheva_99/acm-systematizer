﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Kursovaya_v1
{
    static class BibTexFile
    {
        public static void ReadFromBibtex(string path, HashSet<string> allTags, Books books)
        {
            string patternTag = @"[a-z]+\s=";
            string patternAmp = @"(\\&\\#38;)|(&amp;)";
            string patternLetter = @"\\[""~v']{\\\w}";  //\"{\i}
            string patternLetter3 = @"\\[""~v']{\w}";  //\"{o} 
            string patternLetter2 = @"{\\[']\w}";   //{\'e}
            string patternNewBook = @"@";
            Book newBook = null;

            Console.WriteLine(path);
            try
            {
                using (StreamReader sr = new StreamReader(path,Encoding.UTF8))
                {
                    while (!sr.EndOfStream)
                    {
                        string curLine = sr.ReadLine();

                        if (Regex.IsMatch(curLine, patternNewBook))     //создаем новую книгу
                        {
                            if (newBook != null)
                            {
                                newBook.ID = newBook.GetChangedValue("title") + newBook.GetChangedValue("author") + newBook.GetChangedValue("numpages");
                                books.AddBook(newBook);
                            }
                            newBook = new Book();

                            string pattern = @"[a-z]+";
                            string type = Regex.Match(curLine, pattern).ToString();
                            //books.Last().TypeOfBook = type;
                            newBook.AddTag("publication type", type);
                            allTags.Add("publication type");
                            
                            int indexOfBrake = curLine.IndexOf("{") + 1;
                            //books.Last().Trash = StringParser.Parser(curLine.Substring(indexOfBrake));
                            newBook.Trash = StringParser.Parser(curLine.Substring(indexOfBrake));
                        }
                        else
                        {
                            // очистка строки от "мусора"
                            curLine = Regex.Replace(curLine, patternAmp, "&");    //замена на &

                            Match match;
                            do
                            {
                                int offset = 4;
                                string curPattern = patternLetter;
                                match = Regex.Match(curLine, curPattern);  // замена на букву\"{\i} 
                                if (match.Success)
                                {
                                    string letter = curLine[match.Index + offset].ToString();
                                    Regex rgx = new Regex(curPattern);
                                    curLine = rgx.Replace(curLine, letter, 1);
                                }

                            }
                            while (match.Success);
                           
                            do
                            {
                                int offset = 3;
                                string curPattern = patternLetter2;
                                match = Regex.Match(curLine, curPattern);  // замена на букву {\'e}
                                if (match.Success)
                                {
                                    string letter = curLine[match.Index + offset].ToString();
                                    Regex rgx = new Regex(curPattern);
                                    curLine = rgx.Replace(curLine, letter, 1);
                                }

                            }
                            while (match.Success);

                            do
                            {
                                int offset = 3;
                                string curPattern = patternLetter3;
                                match = Regex.Match(curLine, curPattern);  // замена на букву \"{a} \~{a} \'{a} \v{e}
                                if (match.Success)
                                {
                                    string letter = curLine[match.Index + offset].ToString();
                                    Regex rgx = new Regex(curPattern);
                                    curLine = rgx.Replace(curLine, letter, 1);
                                }

                            }
                            while (match.Success);

                            var bookTag = Regex.Match(curLine, patternTag).ToString();   //ищем в строке тэг

                            if (bookTag != string.Empty)    //если тэг найден
                            {
                                bookTag = bookTag.Remove(bookTag.Length - 1, 1);    //удаляем знак =
                                bookTag = StringParser.Parser(bookTag);

                                int startIndex = curLine.IndexOf('=');  //не все значения записываются в {}
                                var bookValue = curLine.Substring(startIndex + 1);  //получаем подстроку от = и до конца
                                bookValue = bookValue.Replace("}", "");
                                bookValue = bookValue.Replace("{", "");
                                if (bookValue[bookValue.Length-1]==',') bookValue = bookValue.Remove(bookValue.LastIndexOf(","));   //удаляем последнюю запятую
                                if (bookTag!= "publication type") newBook.AddTag(bookTag, bookValue);


                                allTags.Add(bookTag);
                            }
                        }
                    }
                    if (newBook != null) newBook.ID = newBook.GetChangedValue("title") + newBook.GetChangedValue("author") + newBook.GetChangedValue("numpages");
                    books.AddBook(newBook); //добавляем последнюю книгу
                }
        }

            catch (Exception e)
            {
                books = null;
                Console.WriteLine("Путь к файлу на чтение указан неверно");
                Console.WriteLine(e.Message);
                return;
            }
}

        public static void WriteInBibtex(string path, Books books)
        {
            try
            {
                using (var wr = new StreamWriter(path, false))
                {
                    foreach (Book book in books)
                    {
                        wr.WriteLine("@" + book.GetChangedValue("publication type") + "{" + book.Trash);
                        foreach (var tag in book.TagValue)
                        {
                            if (tag.Key != "publication type")
                            {
                                wr.Write(" " + tag.Key + " " + "= {");
                                wr.WriteLine(tag.Value.Item1 + "},");
                            }
                        }
                        wr.WriteLine("}");
                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка создания файла\n" + e.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }
    }
}
