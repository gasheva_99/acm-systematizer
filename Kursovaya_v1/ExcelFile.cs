﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Excel = Microsoft.Office.Interop.Excel;
using Word = Microsoft.Office.Interop.Word;
namespace Kursovaya_v1
{
    static class ExcelFile
    {
        public static void ExportTableToExcel(DataGridView dgvVisualization, bool withChart)
        {
            object misValue = System.Reflection.Missing.Value;
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            try
            {
                xlApp = new Excel.Application();
                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.Add();
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка создания excel файла");
                MessageBox.Show(e.Message);
                //xlApp.Quit();
                return;
            }

            try
            {
                for (int i = 0; i < dgvVisualization.Rows.Count; i++)
                {
                    for (int j = 0; j < dgvVisualization.ColumnCount; j++)
                    {
                        xlWorkSheet.Cells[i + 1, j + 1] = j == 0 ? "'" + dgvVisualization[j, i].Value + "'" : dgvVisualization[j, i].Value;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка экспорта таблицы");
                xlApp.Quit();
                return;
            }
            bool ex = true;
            if (withChart) ex = ExportChartToExcel(dgvVisualization, xlApp, xlWorkBook, xlWorkSheet);
            if (!ex) return;
            xlApp.Visible = true;
            

        }
        private static bool ExportChartToExcel(DataGridView dgvVisualization, Excel.Application xlApp, Excel.Workbook xlWorkBook, Excel.Worksheet xlWorkSheet)
        {
            object misValue = System.Reflection.Missing.Value;
            try
            {
                Excel.Range chartRange;
                Excel.ChartObjects xlCharts = (Excel.ChartObjects)xlWorkSheet.ChartObjects(Type.Missing);
                Excel.ChartObject myChart = (Excel.ChartObject)xlCharts.Add(80, 80, 800, 400);
                Excel.Chart chartPage = myChart.Chart;

                chartPage.HasLegend = true;
                chartPage.Legend.Position = Excel.XlLegendPosition.xlLegendPositionBottom;

                chartPage.HasTitle = true;
                chartPage.ChartTitle.Text = dgvVisualization.Columns[0].HeaderText;

                string b = "B" + dgvVisualization.Rows.Count;

                chartRange = xlWorkSheet.get_Range("A1", b);
                chartPage.SetSourceData(chartRange, misValue);
                chartPage.ChartType = Excel.XlChartType.xlPie;
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка экспорта диаграммы");
                Console.WriteLine(e.Message);
                xlApp.Quit();
                return false;
            }
            return true;
        }
    }
}
