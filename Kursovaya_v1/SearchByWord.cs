﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kursovaya_v1
{
    class SearchByWord<T>:ISearch<T>
        where T:Tuple<string, string>
    {

        string request, reqtag;
        /// <summary>
        /// нахождение списка книг по слову (любой тэг)
        /// </summary>
        /// <param name="request">заданное слово</param>
        /// <returns>Books или null</returns>
        public Books Search(Books b)
        {
            request = StringParser.Parser(request);
            if (String.IsNullOrEmpty(request))
                return null;
            request = request.Replace("*", @"\w*");
            request = request.Replace("+", @"\w+");
            request = request.Replace("?", @"\w");
            return new Books(b.getBooks.Where(x => x.ContainsWord(request, reqtag)).ToList());

        }

        public void setParameters(T parameter)
        {
            Tuple<string, string> parameters = parameter as Tuple<string, string>;
            request = parameters.Item1;
            reqtag = parameters.Item2;

        }
    }
}
