﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Kursovaya_v1
{
    static class WordFile
    {
        public static void Gost(string path, Books books)
        {
            var app = new Word.Application();
            Word.Document doc = null;
            Object falseObj = false;

            //create the document
            try
            {
                doc = app.Documents.Add();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Проблемы с созданием файла word");
                Console.WriteLine(ex.Message);
                app.Quit(ref falseObj);
                return;
            }
            //clear the doc
            var docRange = doc.Content;
            docRange.Delete();

            Word.Paragraph paragraphTitle = doc.Paragraphs.Add();
            paragraphTitle.Range.Text = "\n";

            //start using new font
            paragraphTitle.Range.Font.Name = "Times New Roman";
            paragraphTitle.Range.Font.Size = 14;
            paragraphTitle.Range.Font.Color = Word.WdColor.wdColorBlack;

            //Create a header paragraph
            paragraphTitle.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
            paragraphTitle.Range.Text = "БИБЛИОГРАФИЧЕСКИЙ СПИСОК" + "\n";
            paragraphTitle.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphJustify;

            books.SortByAuthor();

            for (int i = 0; i < books.size(); i++)
            {
                //add paragraphs
                Word.Paragraph paragraph = doc.Paragraphs.Add();

                paragraph.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphJustify;

                //start using new font
                paragraph.Range.Font.Name = "Times New Roman";
                paragraph.Range.Font.Size = 14;
                paragraph.Range.Font.Color = Word.WdColor.wdColorBlack;

                var authorsAndCount = GetAuthorsGost(books[i]);
                string authors = authorsAndCount.Item1;
                int authorsCount = authorsAndCount.Item2;

                string textINFile;
                string booktitle = books[i].GetChangedValue("booktitle");

                string publisher = books[i].GetChangedValue("publisher");
                if (publisher == "Association for Computing Machinery") publisher = "ACM";
                string doi = books[i].GetChangedValue("doi");
                if (doi != null) doi = "doi: " + doi + ".";

                string pages = books[i].GetChangedValue("pages") != null ? books[i].GetChangedValue("pages") : books[i].GetChangedValue("numpages");
                pages = pages.Replace("--", "-");

                string bookType = books[i].GetChangedValue("publication type");  //"publication type"
                int indexPubl = i + 1;
                switch (bookType)
                {
                    
                    case "inbook":
                        string address = books[i].GetChangedValue("address");
                        if (address!=null) { address += ":"; }
                        
                        if (authorsCount <= 3)
                        {
                            textINFile = indexPubl + ". " + authors + " " + books[i].GetChangedValue("booktitle") + ". " +
                                address + " " + publisher + ", " + books[i].GetChangedValue("year") + ". "+pages+" P. ";
                        }
                        else
                        {
                            textINFile = indexPubl + ". " + books[i].GetChangedValue("booktitle") +" / "+ authors + ". " + 
                                address + ": " + publisher + ", " + books[i].GetChangedValue("year") + ". " + pages + " P. ";
                        }
                        break;
                    //конференция
                    case "inproceedings":
                        if (authorsCount <= 3)
                        {
                            textINFile = indexPubl + ". " + authors + " " + books[i].GetChangedValue("title") + " // " +
                                books[i].GetChangedValue("booktitle") + ". " + books[i].GetChangedValue("location") + ": " +
                                publisher + ", " + books[i].GetChangedValue("year") + ". " + " P. " + pages;
                        }
                        else
                        {
                            textINFile = indexPubl + ". " + books[i].GetChangedValue("title") + " / " + authors + " // " +
                                books[i].GetChangedValue("booktitle") + ". " + books[i].GetChangedValue("location") + ": " +
                                publisher + ", " + books[i].GetChangedValue("year") + ". " + " P. " + pages;
                        }
                        break;
                    //журнал
                    default:
                        string volume = books[i].GetChangedValue("volume");
                        if (authorsCount<=3)
                        {
                            textINFile = indexPubl + ". " + authors + " " + books[i].GetChangedValue("title") + " // " +
                                books[i].GetChangedValue("journal") + ". " + books[i].GetChangedValue("year") + ". V. " +
                                volume + ". N " + books[i].GetOriginalValue("number") + ". P. " + pages;
                        }
                        else
                        {
                            textINFile = indexPubl + ". " + books[i].GetChangedValue("title")+" / " + authors + " // " +
                                books[i].GetChangedValue("journal") + ". " + books[i].GetChangedValue("year") + ". V. " +
                                volume + ". N " + books[i].GetOriginalValue("number") + ". C. " + pages;
                        }
                        break;

                }

                textINFile=textINFile.Replace("..", ".");
                textINFile=StringParser.Parser(textINFile);
                try
                {
                    paragraph.Range.Text = textINFile;
                    paragraph.Range.InsertParagraphAfter();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Проблемы с записью в Word");
                    Console.WriteLine(ex.Message);
                    doc.Close();
                    app.Quit(ref falseObj);
                    return;
                }

            }
            app.Visible = true;
        }

        private static Tuple<string, string> GetFIO(string author)
        {
            string[] surnameName = author.Split(',');

            char firstNameLetter = ' ';
            char otchestvo = ' ';

            //если есть не только фамилия
            if (surnameName.Length > 1)
            {
                surnameName[1] = surnameName[1].Trim();
                //получаем букву имени
                firstNameLetter = surnameName[1][0];

                //получаем букву отчества
                int indexOfSpace = surnameName[1].IndexOf(' ');
                if (indexOfSpace >= 0)
                {
                    otchestvo = surnameName[1][indexOfSpace + 1];
                }
            }
            string IO = (firstNameLetter != ' ' ? firstNameLetter.ToString() + "." : "") + (otchestvo != ' ' ? " "+ otchestvo.ToString() + "." : "");
            return new Tuple<string, string>(surnameName[0].Trim(), IO);
        }
        private static Tuple<string, int> GetAuthorsGost(Book book)
        {
            //get authors
            string authors = book.GetChangedValue("author");
            if (authors == null) return new Tuple<string, int>("", 0);
            //получили массив ФИО полных
            string[] arrAuthors = authors.Split(new string[] { " and " }, System.StringSplitOptions.RemoveEmptyEntries);
            for (int j = 0; j < arrAuthors.Length; j++)
            {
                //отделили фамилию от инициалов
                var IO = GetFIO(arrAuthors[j]);
                string fullName;
                if (arrAuthors.Length < 4) fullName = IO.Item1 + " " + IO.Item2;
                else fullName = IO.Item2+" " + IO.Item1;
                arrAuthors[j] = fullName;
            }
            authors = arrAuthors[0] + " ";
            if (arrAuthors.Length > 1)
            {
                authors = "";
                for (int k = 0; k < arrAuthors.Length && k < 4; k++)
                {
                    authors += arrAuthors[k] + ", ";
                }
                //удаляем последнюю запятую
                authors= authors.Remove(authors.LastIndexOf(","), 1);
                if (arrAuthors.Length > 4) authors += "et al. ";
            }
            Console.WriteLine(authors);
            return new Tuple<string, int>(authors, arrAuthors.Length);
        }
        private static Tuple<string, int> GetAuthorsIEEE(Book book)
        {
            //get authors
            string authors = book.GetChangedValue("author");
            if (authors == null) return new Tuple<string, int>("", 0);
            string[] arrAuthors = authors.Split(new string[] { " and " }, System.StringSplitOptions.RemoveEmptyEntries);
            for (int j = 0; j < arrAuthors.Length; j++)
            {
                //отделили фамилию от инициалов
                var IO = GetFIO(arrAuthors[j]);
                string fullName;
                fullName = IO.Item2 + " " + IO.Item1;
                arrAuthors[j] = fullName;
            }

            authors = arrAuthors[0] + ", ";
            if (arrAuthors.Length > 1)
            {
                authors = "";

                if (arrAuthors.Length == 2)
                {
                    authors += arrAuthors[0] + " and " + arrAuthors[1] + ", ";
                }
                else
                if (arrAuthors.Length > 6)
                {
                    authors += arrAuthors[0] + " et al., ";
                }
                else
                {
                    for (int k = 0; k < arrAuthors.Length && k < 5; k++)
                    {
                        if (k == arrAuthors.Length - 1)
                            authors += "and ";
                        authors += arrAuthors[k] + ", ";
                    }
                }
            }
            return new Tuple<string, int>(authors, arrAuthors.Length);
        }

        public static void IEEE(string path, Books books, Dictionary<string, string> abbr, bool useAbbr)
        {
            var app = new Word.Application();
            Word.Document doc = null;
            Object falseObj = false;

            //create the document
            try
            {
                doc = app.Documents.Add();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Проблемы с созданием файла word");
                Console.WriteLine(ex.Message);
                app.Quit(ref falseObj);
                return;
            }
            //clear the doc
            var docRange = doc.Content;
            docRange.Delete();

            Word.Paragraph paragraphTitle = doc.Paragraphs.Add();
            paragraphTitle.Range.Text = "\n";

            //start using new font
            paragraphTitle.Range.Font.Name = "Times New Roman";
            paragraphTitle.Range.Font.Size = 14;
            paragraphTitle.Range.Font.Color = Word.WdColor.wdColorBlack;

            //Create a header paragraph
            paragraphTitle.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
            paragraphTitle.Range.Text = "БИБЛИОГРАФИЧЕСКИЙ СПИСОК" + "\n";
            paragraphTitle.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphJustify;


            for (int i = 0; i < books.size(); i++)
            {
                //add paragraphs
                Word.Paragraph paragraph = doc.Paragraphs.Add();

                paragraph.Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphJustify;

                //start using new font
                paragraph.Range.Font.Name = "Times New Roman";
                paragraph.Range.Font.Size = 14;
                paragraph.Range.Font.Color = Word.WdColor.wdColorBlack;

                var authorsAndCount = GetAuthorsIEEE(books[i]);
                string authors = authorsAndCount.Item1;
                int authorsCount = authorsAndCount.Item2;

                string textINFile;
                string booktitle = books[i].GetChangedValue("booktitle");
                string[] publWords;
                if (booktitle != null && useAbbr)
                {
                    publWords = booktitle.Split(' ');
                    booktitle = "";
                    string abbrStr = "";
                    Func<string, string> findAbbr = (x) => { if (abbr.TryGetValue(x.Trim(), out abbrStr)) return abbrStr; else return x; };
                    var ready = publWords.Select(x => findAbbr(x)).ToArray();
                    foreach (string str in ready) booktitle += str + " ";
                    booktitle = booktitle.Trim();
                }

                string journal = books[i].GetChangedValue("journal");
                string publisher = books[i].GetChangedValue("publisher");
                if (publisher == "Association for Computing Machinery") publisher = "ACM";
                //string doi = books[i].GetChangedValue("doi");
                //if (doi != null) doi = "doi: " + doi + ".";

                string pages = books[i].GetChangedValue("pages") != null ? books[i].GetChangedValue("pages") : books[i].GetChangedValue("numpages");
                pages = pages.Replace("--", "-");

                string bookType = books[i].GetChangedValue("publication type");//books[i].TypeOfBook;
                int indexPubl = i + 1;
                switch (bookType)
                {
                    //конференция
                    case "inproceedings":
                        textINFile = "[" + indexPubl + "] " +
                                authors + "\"" + books[i].GetChangedValue("title") + ",\" in " + booktitle + ", " +
                                books[i].GetChangedValue("year") + ", pp. " + pages + ".";
                        break;
                    case "article":
                        string month = books[i].GetChangedValue("month");
                        string volume = books[i].GetChangedValue("volume") != null ? books[i].GetChangedValue("volume") : books[i].GetChangedValue("volume");
                        month = books[i].GetChangedValue("month")[0].ToString().ToUpper() + books[i].GetChangedValue("month").Remove(0, 1);
                        textINFile = "[" + indexPubl + "] " +
                            authors + "\"" + books[i].GetChangedValue("title") + ",\" " + journal + ", vol. " + volume + ", no. " +
                            books[i].GetChangedValue("number") + ", pp. " + pages + ", " + month + ". " +
                            books[i].GetChangedValue("year")+".";
                        break;
                    //книга
                    default:
                        string title = books[i].GetChangedValue("title");
                        booktitle = books[i].GetChangedValue("booktitle");
                        if (title != null) title = " \"" + title + ",\"";
                        string address = books[i].GetChangedValue("address");
                        if (address != null)
                        {
                            int USAIndex = address.IndexOf("USA");
                            if (USAIndex != -1) address = address.Remove(address.IndexOf("USA")-2, 5);    //удаляем страну и , до этого
                            string[] abbrCountry = address.Split(',');
                            if (abbrCountry.Length > 1 && USAIndex!=-1) address = abbrCountry[0];   //удаляем сокращение города
                            address += ": ";
                        }
                        string chapter = books[i].GetChangedValue("chapter");
                        if (chapter != null) chapter = "ch." + chapter + ", ";
                        string section = books[i].GetChangedValue("section");
                        if (section != null) section = "sec." + section;

                        textINFile = "[" + indexPubl + "] " +
                            authors + title + (title != null ? " in " : " ") + booktitle + ". " +
                            address +
                            publisher + ", " + 
                            books[i].GetChangedValue("year") + chapter + section +
                            ", pp. " +
                            pages + ".";
                        break;

                }
                textINFile = StringParser.Parser(textINFile);
                try
                {
                    paragraph.Range.Text = textINFile;
                    if (bookType == "article")
                    {
                        Word.Range parRange = doc.Paragraphs.Last.Range;
                        int start = parRange.Text.IndexOf(journal);
                        parRange.MoveStart(Word.WdUnits.wdCharacter, start);
                        parRange.SetRange(parRange.Start, parRange.Start + journal.Length);
                        parRange.Italic = 1;
                    }
                    else if (bookType == "inproceedings"||bookType=="inbook")
                    {
                        Word.Range parRange = doc.Paragraphs.Last.Range;
                        int start = parRange.Text.IndexOf(booktitle);
                        parRange.MoveStart(Word.WdUnits.wdCharacter, start);
                        parRange.SetRange(parRange.Start, parRange.Start + booktitle.Length);
                        parRange.Italic = 1;
                    }
                    int indexEtAl = authors.IndexOf("et al");
                    if (indexEtAl != -1)
                    {
                        Word.Range parRange = doc.Paragraphs.Last.Range;
                        int start = parRange.Text.IndexOf("et al");
                        parRange.MoveStart(Word.WdUnits.wdCharacter, start);
                        parRange.SetRange(parRange.Start, parRange.Start + 5);
                        parRange.Italic = 1;
                    }
                    paragraph.Range.InsertParagraphAfter();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Проблемы с записью в Word");
                    Console.WriteLine(ex.Message);
                    doc.Close();
                    app.Quit(ref falseObj);
                    return;
                }

            }
            app.Visible = true;
        }

        public static bool Export_Data_To_Word(DataGridView DGV, string filename)
        {
            if (DGV.Rows.Count != 0)
            {
                int RowCount = DGV.Rows.Count;
                int ColumnCount = DGV.Columns.Count;
                Object[,] DataArray = new object[RowCount + 1, ColumnCount + 1];

                //add rows
                int r = 0;
                for (int c = 0; c <= ColumnCount - 1; c++)
                {
                    for (r = 0; r <= RowCount - 1; r++)
                    {
                        DataArray[r, c] = DGV.Rows[r].Cells[c].Value;
                    } //end row loop
                } //end column loop

                Word.Document oDoc = new Word.Document();

                //page orintation
                oDoc.PageSetup.Orientation = Word.WdOrientation.wdOrientLandscape;


                dynamic oRange = oDoc.Content.Application.Selection.Range;
                string oTemp = "";
                for (r = 0; r <= RowCount - 1; r++)
                {
                    for (int c = 0; c <= ColumnCount - 1; c++)
                    {
                        oTemp = oTemp + DataArray[r, c] + "\t";
                    }
                }

                //table format
                oRange.Text = oTemp;

                object Separator = Word.WdTableFieldSeparator.wdSeparateByTabs;
                object ApplyBorders = true;
                object AutoFit = true;
                object AutoFitBehavior = Word.WdAutoFitBehavior.wdAutoFitContent;

                oRange.ConvertToTable(ref Separator, ref RowCount, ref ColumnCount,
                                      Type.Missing, Type.Missing, ref ApplyBorders,
                                      Type.Missing, Type.Missing, Type.Missing,
                                      Type.Missing, Type.Missing, Type.Missing,
                                      Type.Missing, ref AutoFit, ref AutoFitBehavior, Type.Missing);

                oRange.Select();

                oDoc.Application.Selection.Tables[1].Select();
                oDoc.Application.Selection.Tables[1].Rows.AllowBreakAcrossPages = 0;
                oDoc.Application.Selection.Tables[1].Rows.Alignment = 0;
                oDoc.Application.Selection.Tables[1].Rows[1].Select();
                oDoc.Application.Selection.InsertRowsAbove(1);
                oDoc.Application.Selection.Tables[1].Rows[1].Select();

                //add header row manually
                for (int c = 0; c <= ColumnCount - 1; c++)
                {
                    oDoc.Application.Selection.Tables[1].Cell(1, c + 1).Range.Text = DGV.Columns[c].HeaderText;
                }

                oDoc.Application.Selection.Tables[1].Borders.InsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;
                oDoc.Application.Selection.Tables[1].Borders.OutsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;
                oDoc.Application.Selection.Tables[1].Rows.Last.Delete();

                oDoc.Application.Selection.Tables[1].Range.Font.Name = "Times New Roman";
                oDoc.Application.Selection.Tables[1].Range.Font.Size = 12;

                oDoc.Application.Visible = true;
                

            }
            return true;
        }
    }
}
