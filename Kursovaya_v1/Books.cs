﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kursovaya_v1
{
    //Коллекция книг
    public class Books : IEnumerable
    {
        List<Book> books;
        public Books()
        {
            books = new List<Book>();
        }
        public Books(List<Book> books)
        {
            this.books = books;
        }
        public List<Book> getBooks { get=> books; }

        public int size()
        {
            return books.Count();
        }
        public void AddBook(Book book)
        {
            if (book == null) return;
            //при добавлении в коллекцию проверяется уникальность книги (ид)
            if (books.Any(x => x.ID == book.ID)) return;
            //проверяем релевантность книги
            if (Relevance.Relevant(book))
                books.Add(book);
        }
        public Book GetBookById(string id)
        {
            var reqBook = books.Where(x => x.ID == id).ToList();
            return reqBook[0];
        }
        
        public IEnumerator GetEnumerator()
        {
            return books.GetEnumerator();
        }

        public Book this[int index]
        {
            get => books[index];
            set => books[index] = value;
        }

        public void SortByAuthor()
        {
            Func<string, string, int> comparator = (x, y) =>
            {
                if (x == null) return 1;
                if (y == null) return -1;
                return x.CompareTo(y);
            };
            books.Sort((x, y) => comparator(x.GetChangedValue("author"), y.GetChangedValue("author")));
        }
    }
}
