﻿namespace Kursovaya_v1
{
    partial class DiagramForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DiagramForm));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.cbParameter = new System.Windows.Forms.ToolStripComboBox();
            this.cbVisualization = new System.Windows.Forms.ToolStripComboBox();
            this.cbType = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btLabels = new System.Windows.Forms.ToolStripButton();
            this.btCount = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.cbSort = new System.Windows.Forms.ToolStripComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvVisualization = new System.Windows.Forms.DataGridView();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.btExcel = new System.Windows.Forms.ToolStripButton();
            this.btWord = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVisualization)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cbParameter,
            this.cbVisualization,
            this.cbType,
            this.toolStripSeparator3,
            this.btLabels,
            this.btCount,
            this.toolStripSeparator1,
            this.cbSort});
            this.toolStrip1.Location = new System.Drawing.Point(0, 25);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.ShowItemToolTips = false;
            this.toolStrip1.Size = new System.Drawing.Size(800, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // cbParameter
            // 
            this.cbParameter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbParameter.Name = "cbParameter";
            this.cbParameter.Size = new System.Drawing.Size(121, 25);
            this.cbParameter.SelectedIndexChanged += new System.EventHandler(this.CbParameter_SelectedIndexChanged);
            // 
            // cbVisualization
            // 
            this.cbVisualization.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVisualization.Name = "cbVisualization";
            this.cbVisualization.Size = new System.Drawing.Size(121, 25);
            this.cbVisualization.SelectedIndexChanged += new System.EventHandler(this.CbVisualization_SelectedIndexChanged);
            // 
            // cbType
            // 
            this.cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(121, 25);
            this.cbType.SelectedIndexChanged += new System.EventHandler(this.CbType_SelectedIndexChanged);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // btLabels
            // 
            this.btLabels.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btLabels.Image = ((System.Drawing.Image)(resources.GetObject("btLabels.Image")));
            this.btLabels.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btLabels.Name = "btLabels";
            this.btLabels.Size = new System.Drawing.Size(114, 22);
            this.btLabels.Text = "Показать названия";
            this.btLabels.Click += new System.EventHandler(this.BtLabelBottom_Click);
            // 
            // btCount
            // 
            this.btCount.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btCount.Image = ((System.Drawing.Image)(resources.GetObject("btCount.Image")));
            this.btCount.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btCount.Name = "btCount";
            this.btCount.Size = new System.Drawing.Size(102, 22);
            this.btCount.Text = "Показать кол-во";
            this.btCount.Click += new System.EventHandler(this.BtCount_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // cbSort
            // 
            this.cbSort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSort.Name = "cbSort";
            this.cbSort.Size = new System.Drawing.Size(121, 25);
            this.cbSort.SelectedIndexChanged += new System.EventHandler(this.CbSort_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvVisualization);
            this.panel1.Controls.Add(this.chart1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 400);
            this.panel1.TabIndex = 1;
            // 
            // dgvVisualization
            // 
            this.dgvVisualization.AllowUserToAddRows = false;
            this.dgvVisualization.AllowUserToDeleteRows = false;
            this.dgvVisualization.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVisualization.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvVisualization.Location = new System.Drawing.Point(0, 0);
            this.dgvVisualization.Name = "dgvVisualization";
            this.dgvVisualization.Size = new System.Drawing.Size(800, 400);
            this.dgvVisualization.TabIndex = 1;
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(0, 0);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(800, 400);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // toolStrip2
            // 
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btExcel,
            this.btWord});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.ShowItemToolTips = false;
            this.toolStrip2.Size = new System.Drawing.Size(800, 25);
            this.toolStrip2.TabIndex = 2;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // btExcel
            // 
            this.btExcel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btExcel.Image = ((System.Drawing.Image)(resources.GetObject("btExcel.Image")));
            this.btExcel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btExcel.Name = "btExcel";
            this.btExcel.Size = new System.Drawing.Size(145, 22);
            this.btExcel.Text = "Экспорт таблицы в Excel";
            this.btExcel.Click += new System.EventHandler(this.ToolStripButton1_Click);
            // 
            // btWord
            // 
            this.btWord.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btWord.Image = ((System.Drawing.Image)(resources.GetObject("btWord.Image")));
            this.btWord.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btWord.Name = "btWord";
            this.btWord.Size = new System.Drawing.Size(148, 22);
            this.btWord.Text = "Экспорт таблицы в Word";
            this.btWord.Click += new System.EventHandler(this.ToolStripButton1_Click);
            // 
            // DiagramForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.toolStrip2);
            this.Name = "DiagramForm";
            this.Text = "Диаграмма";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVisualization)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripComboBox cbParameter;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.ToolStripComboBox cbType;
        private System.Windows.Forms.ToolStripButton btLabels;
        private System.Windows.Forms.ToolStripButton btCount;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripComboBox cbSort;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripComboBox cbVisualization;
        private System.Windows.Forms.DataGridView dgvVisualization;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton btExcel;
        private System.Windows.Forms.ToolStripButton btWord;
    }
}